import 'dart:convert';
import 'package:http/http.dart' as http;

class StudentOperations {
  static Future<List<dynamic>> getAllStudents() async {
    final response =
        await http.get(Uri.parse('https://jsonplaceholder.typicode.com/users'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load students');
    }
  }

  static Future<Map<String, dynamic>> getStudentById(int id) async {
    final response = await http
        .get(Uri.parse('https://jsonplaceholder.typicode.com/users/$id'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load student');
    }
  }

  static Future<bool> createStudent(Map<String, dynamic> studentData) async {
    final response = await http.post(
      Uri.parse('https://jsonplaceholder.typicode.com/users'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(studentData),
    );
    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> updateStudent(
      int id, Map<String, dynamic> updatedStudentData) async {
    try {
      print('updatstudent executed...');
      final response = await http.put(
        Uri.parse('https://jsonplaceholder.typicode.com/users/$id'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(updatedStudentData),
      );
      print(response);
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print('cilad ayaa dhacdey. ${e}');
      return false;
    }
  }

  static Future<http.Response> deleteStudent(int id) async {
    final response = await http
        .delete(Uri.parse('https://jsonplaceholder.typicode.com/users/$id'));
    return response;
  }
}
