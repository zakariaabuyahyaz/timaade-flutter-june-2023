import 'package:flutter/material.dart';
import 'package:layoutexamples/Univsersity.dart';
import 'package:layoutexamples/liststudent.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: DisplayList(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class layoutExampleOne extends StatelessWidget {
  const layoutExampleOne({super.key});

  @override
  Widget build(BuildContext context) {
    // visual elements
    return Scaffold(
      appBar: AppBar(
        title: Text('Layout Example One'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        margin: EdgeInsets.all(5),
        // padding: EdgeInsets.all(15),
        // alignment: Alignment.bottomRight,
        color: Colors.blue,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  color: Colors.red,
                  width: 80,
                  height: 80,
                  child: Text('ONE'),
                ),
                Container(
                  alignment: Alignment.center,
                  color: Colors.green,
                  width: 80,
                  height: 80,
                  child: Text('TWO'),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  color: Colors.yellow,
                  width: 80,
                  height: 80,
                  child: Text('THREE'),
                ),
                Container(
                  alignment: Alignment.center,
                  color: Colors.white,
                  width: 80,
                  height: 80,
                  child: Text('FOUR'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ButtonEx extends StatelessWidget {
  const ButtonEx({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Example'),
      ),
      body: Column(
        children: [
          Text('Heelo'),
          ElevatedButton(
            onPressed: () {
              print('email sent');
            },
            child: Text('Send email'),
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.redAccent,
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                )),
          ),
          TextButton(
            onPressed: () {
              print('text button');
            },
            // style: TextButton.styleFrom(
            //   textStyle: TextStyle(
            //     backgroundColor: Colors.blue,

            //   ),
            // ),
            child: Text(
              'Yes, Enable',
              style: TextStyle(color: Colors.redAccent),
            ),
          ),
        ],
      ),
    );
  }
}

class moreexamples extends StatefulWidget {
  const moreexamples({super.key});

  @override
  State<moreexamples> createState() => _moreexamplesState();
}

class _moreexamplesState extends State<moreexamples> {
  int counter = 0;

  void _clickHandler() {
    counter++;
    print('The button is pressed ' + (counter).toString() + ' time(s)');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Text Examples'),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Contract',
                style: TextStyle(fontSize: 20.0, color: Colors.redAccent),
              )
            ],
          ),
          Container(
            alignment: Alignment.center,
            color: Colors.blue,
            padding: EdgeInsets.all(10.0),
            width: double.infinity,
            height: 200,
            child: Text(
                'This agreeement is between the software company and its clients.  This agreeement is between the software company and its clients. This agreeement is between the software company and its clients. Do you Agree terms and policies?'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  TextButton(onPressed: () {}, child: Text('No, I disagree')),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  TextButton(onPressed: () {}, child: Text('Yes, I agree')),
                ],
              )
            ],
          ),
        ],
      ),
      //  Column(
      // children: [
      //   Container(
      //     width: double.infinity,
      //     height: 200,
      //     padding: EdgeInsets.all(10.0),
      //     child: Text(
      //       'Buttons',
      //       textAlign: TextAlign.center,
      //       softWrap: false,
      //       style: TextStyle(
      //           color: Colors.blue,
      //           fontSize: 15.0,
      //           fontWeight: FontWeight.bold,
      //           fontFamily: 'Arial'),
      //     ),
      //   ),
      //   ElevatedButton(
      //     onPressed: _clickHandler,
      //     child: Text('Send Email'),
      //     style: ElevatedButton.styleFrom(
      //       backgroundColor: Colors.redAccent,
      //       elevation: 4,
      //       shape: RoundedRectangleBorder(
      //           borderRadius: BorderRadius.circular(20)),
      //       textStyle: TextStyle(
      //         color: Colors.white,
      //       ),
      //     ),
      //   ),
      //   TextButton(onPressed: _clickHandler, child: Text('Yes, I Agree'))
      // ],
      // ),
    );
  }
}

class MoreExamples extends StatefulWidget {
  const MoreExamples({super.key});

  @override
  State<MoreExamples> createState() => _MoreExamplesState();
}

class _MoreExamplesState extends State<MoreExamples> {
  void calculate() {
    int a = 9;
    int b = 1;
    int c = a + b + a - 1;
    print('the sum is: ' + c.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('More Examples')),
      body: Container(
        color: Colors.blue,
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Login',
              textAlign: TextAlign.center,
              softWrap: false,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.redAccent,
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  textStyle: TextStyle(fontSize: 25.0)),
              onPressed: calculate,
              child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [Icon(Icons.lock_outline), Text('Login Here')]),
            ),
          ],
        ),
      ),
    );
  }
}

class FormExamples extends StatefulWidget {
  const FormExamples({super.key});

  @override
  State<FormExamples> createState() => _FormExamplesState();
}

class _FormExamplesState extends State<FormExamples> {
//config for dropdown
  String _initialValue = "Select";
  List<String> items = ["Select", "Timaade", "UOH", "Golis", "Alpha"];
  String? selectedValue;

  bool maleCheckbox = false;
  bool femaleChekbox = false;

  final _formkey = GlobalKey<FormState>();
  TextEditingController _fullnameController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  TextEditingController _usernameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Create Account')),
      body: ListView(
        padding: const EdgeInsets.all(5),
        children: <Widget>[
          Container(
            // color: Colors.blue,
            // width: double.infinity,
            // height: double.infinity,
            // padding: EdgeInsets.all(20),
            child: Form(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              key: _formkey,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    'Create Your Account',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: _fullnameController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter Fullname";
                      }

                      if (value.length < 14) {
                        return "Invalid Name";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.person_2_outlined,
                        color: Colors.blue,
                      ),
                      labelText: 'Full Name',
                      hintText: 'Enter Until your fourth name',
                      hintStyle: TextStyle(
                        fontSize: 17,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: _mobileController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter Mobile";
                      }

                      if (value.length != 9) {
                        return "Mobile should 9 digits long";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.phone,
                        color: Colors.blue,
                      ),
                      labelText: 'Mobile',
                      hintText: 'Enter 9 digits',
                      hintStyle: TextStyle(
                        fontSize: 17,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: _usernameController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Enter Username";
                      }

                      if (value.length != 10) {
                        return "Username should 10 chars";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.lock_open_outlined,
                        color: Colors.blue,
                      ),
                      labelText: 'Username',
                      hintText: 'Enter minimum 10 chars',
                      hintStyle: TextStyle(
                        fontSize: 17,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      labelText: 'University',
                      prefixIcon:
                          Icon(Icons.house_outlined, color: Colors.blue),
                    ),
                    validator: (value) {
                      if (value == "Select") {
                        return "Univeristy is required";
                      }
                      return null;
                    },
                    onChanged: (value) {
                      selectedValue = value ?? "";
                    },
                    value: _initialValue,
                    items: items.map((element) {
                      return DropdownMenuItem<String>(
                          child: Text(element), value: element);
                    }).toList(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text('Gender:'),
                  Row(
                    children: [
                      Checkbox(
                        activeColor: Colors.orangeAccent,
                        value: maleCheckbox,
                        onChanged: (value) {
                          setState(() {
                            maleCheckbox = value!;
                          });
                        },
                      ),
                      Text('Male')
                    ],
                  ),
                  Row(
                    children: [
                      Checkbox(
                        activeColor: Colors.orangeAccent,
                        value: femaleChekbox,
                        onChanged: (value) {
                          setState(() {
                            femaleChekbox = value!;
                          });
                        },
                      ),
                      Text('Female')
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    title: Text('Male'),
                    subtitle: Text('Your Description goes here'),
                    secondary: Icon(
                      Icons.person_2_outlined,
                      color: Colors.green,
                    ),
                    value: maleCheckbox,
                    onChanged: (value) {
                      setState(() {
                        maleCheckbox = value!;
                      });
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 4,
                    color: Colors.orangeAccent,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Notice !'),
                            TextButton(
                              child: Text('Yes, Notified'),
                              onPressed: () {},
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          if (_formkey.currentState!.validate()) {
                            print('validation passed');
                          } else {
                            print('validatieon failed');
                          }
                        },
                        child: Row(
                          children: [
                            Text('Submit'),
                            SizedBox(
                              width: 5,
                            ),
                            Icon(
                              Icons.send_outlined,
                              color: Colors.white,
                            ),
                          ],
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blue,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ListExampleOne extends StatefulWidget {
  const ListExampleOne({super.key});

  @override
  State<ListExampleOne> createState() => _ListExampleOneState();
}

class _ListExampleOneState extends State<ListExampleOne> {
  var items = <University>[
    University(
        name: 'Timaade',
        Remarks: 'This is mostly visited university by students',
        Image: "https://sampleimages/200/44",
        Fee: "200"),
    University(
        name: 'UOH',
        Remarks: 'UOH is located in biibsi area',
        Image: "https://sampleimages/200/44",
        Fee: "150"),
    University(
        name: 'Golis',
        Remarks: 'This is owned by saed',
        Image: "https://sampleimages/200/44",
        Fee: "200"),
    University(
        name: 'Alpha',
        Remarks: 'Is Ethiopian origined university',
        Image: "https://sampleimages/200/44",
        Fee: "250"),
    University(
        name: 'New Generation',
        Remarks: 'This is mostly visited university by students',
        Image: "https://sampleimages/200/44",
        Fee: "300"),
    University(
        name: 'Admas',
        Remarks: 'This is mostly visited university by students',
        Image: "https://sampleimages/200/44",
        Fee: "50"),
    University(
        name: 'Amoud',
        Remarks: 'It is located in Borama Reggion',
        Image: "https://sampleimages/200/44",
        Fee: "50"),
    University(
        name: 'Edna',
        Remarks: 'This is mostly visited university by students',
        Image: "https://sampleimages/200/44",
        Fee: "1000")
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Universities')),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Card(
            margin: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
            elevation: 4,
            color: Color.fromRGBO(255, 224, 178, 1),
            child: ListTile(
              title: Text(items[index]!.name),
              subtitle: Column(
                children: [
                  Text(items[index]!.Remarks),
                  Row(
                    children: [
                      Text(
                        'Fee:',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(items[index]!.Fee),
                    ],
                  )
                ],
              ),
              leading: Icon(Icons.house_outlined),
              trailing: Icon(Icons.menu_outlined),
            ),
          );
        },
      ),
    );
  }
}
