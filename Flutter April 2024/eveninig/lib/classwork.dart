import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Classwork1 extends StatelessWidget {
  const Classwork1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Classwork 1'),
      ),
      body: Column(
        children: [
          Container(
            width: 150,
            height: 150,
            color: Colors.red,
            margin: EdgeInsets.only(bottom: 10),
          ),
          Container(
            width: 150,
            height: 150,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}

class Classwork2 extends StatelessWidget {
  const Classwork2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Classwork 2'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(5),
            width: 100,
            height: 100,
            color: Colors.red,
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.all(5),
                width: 100,
                height: 100,
                color: Colors.red,
              ),
              Container(
                margin: EdgeInsets.all(5),
                width: 100,
                height: 100,
                color: Colors.red,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.all(5),
            width: 100,
            height: 100,
            color: Colors.red,
            child: Text('18'),
          ),
        ],
      ),
    );
  }
}

class Classwork3 extends StatelessWidget {
  const Classwork3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Classwork 3'),
      ),
      body: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        width: double.infinity,
        height: 200,
        color: Colors.purple.shade300,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Raisi Death',
              style: TextStyle(fontSize: 25, color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Madaxweynaha dalka Iran Raisi ayaa ku geeriyoodey shild diyaaradeed oo ka dhacey ......',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  Icons.share_outlined,
                  size: 30,
                  color: Colors.white,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
