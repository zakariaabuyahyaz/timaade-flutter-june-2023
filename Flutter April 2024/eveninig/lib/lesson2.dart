import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson2 extends StatelessWidget {
  const Lesson2({super.key});

  @override
  Widget build(BuildContext context) {
    String username = "Ahmed";
    //responsible to creating UI
    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson 2'),
      ),
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: 250,
                color: Colors.amber,
                child: Text(
                  "$username's Profile",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                "18 May",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Madaxweynaha soomaaliland ayaa maanta ka hadley munaasibada 18 may ee sanad guurada 33 ee.......',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.amber,

                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  // textStyle: TextStyle(color: Colors.black)
                ),
                onPressed: () {
                  int a = 90;
                  int b = 1;
                  int total = a + b;
                  print(
                      'you pressed me. total is $total'); // display in the console
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      color: Colors.black,
                      Icons.comment_outlined,
                      size: 25,
                    ),
                    Text(
                      'Comment',
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              ),
              TextButton(
                onPressed: () {
                  int a = 90;
                  int b = 1;
                  int total = a + b;
                  print(
                      'you pressed me. total is $total'); // display in the console
                },
                child: Text(
                  'Yes',
                  // style: TextStyle(color: Colors.black),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('200'),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.thumb_up_alt_outlined,
                    color: Colors.orange,
                    size: 20,
                  ),
                ],
              ),
            ],
          ),
          Positioned(
            left: 170,
            top: 223,
            child: Container(
              width: 50,
              height: 50,
              color: Colors.black,
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(
          Icons.share,
          color: Colors.orange,
          size: 25,
        ),
      ),
      // body: Column(
      //   children: [
      //     Container(
      //       padding: EdgeInsets.all(10),
      //       width: double.infinity, //take all available widht size of the devie
      //       height: 250,
      //       color: Colors.purple,
      //       child: Column(
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: [
      //           Text(
      //             '18 May',
      //             style: TextStyle(
      //                 color: Colors.white,
      //                 fontSize: 25,
      //                 fontWeight: FontWeight.bold),
      //           ),
      //           Text(
      //             'Madaxweynaha soomaaliland ayaa maanta ka hadley munaasibada 18 may ee sanad guurada 33 ee.......',
      //             style: TextStyle(
      //               color: Colors.white,
      //               fontSize: 15,
      //             ),
      //           ),
      //           Row(
      //             mainAxisAlignment: MainAxisAlignment.end,
      //             children: [
      //               ElevatedButton(onPressed: () {}, child: Text('Read More')),
      //             ],
      //           ),
      //         ],
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}
