import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lessonfive extends StatefulWidget {
  const Lessonfive({super.key});

  @override
  State<Lessonfive> createState() => _LessonfiveState();
}

class _LessonfiveState extends State<Lessonfive> {
  final _fkey = GlobalKey<FormState>();

  List<String> countries = ['Select', 'Somaliland', 'Somalia', 'Ethiopia'];
  String selectedCountry = 'Select';

  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(
          'Sign Up',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30, 50, 30, 5),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _fkey,
          child: ListView(
            children: [
              TextFormField(
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Fullname is Required";
                  }

                  if (newvalue!.length <= 10) {
                    return "Invalid Name";
                  }
                  return null;
                },
                keyboardType: TextInputType.name,
                maxLength: 35,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.person,
                      color: Colors.purple,
                    ),
                    labelText: 'Fullname',
                    hintText: 'Enter first, second and third names',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Mobile is Required";
                  }

                  if (newvalue!.length != 9) {
                    return "Invalid Mobile";
                  }
                  return null;
                },
                keyboardType: TextInputType.phone,
                maxLength: 9,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.phone,
                      color: Colors.purple,
                    ),
                    labelText: 'Mobile',
                    hintText: 'Enter 9 digits, EX: 63/65xxxxxxx',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              DropdownButtonFormField(
                validator: (newvalue) {
                  if (newvalue == 'Select') {
                    return "Country is Required";
                  }
                  return null;
                },
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Country',
                    prefixIcon: Icon(
                      Icons.location_on,
                      color: Colors.purple,
                    )),
                value: selectedCountry,
                onChanged: (newvalue) {
                  setState(() {
                    selectedCountry = newvalue!;
                  });
                },
                items: countries.map((e) {
                  return DropdownMenuItem(
                    child: Text(e),
                    value: e,
                  );
                }).toList(),
              ),
              SizedBox(
                height: 15,
              ),
              CheckboxListTile(
                title: Text('Confirm Policy?'),
                subtitle: Text(
                    'I agree to the software company all the terms regulating the use of this app. I also agree data & privacy policies.'),
                value: isChecked,
                onChanged: (newvalue) {
                  setState(() {
                    isChecked = newvalue!;
                  });
                },
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.purple),
                    onPressed: _submitdata,
                    child: Text('Submit'),
                  ),
                ],
              ),

              // Row(
              //   children: [
              //     Checkbox(
              //       value: isChecked,
              //       onChanged: (newvalue) {
              //         setState(() {
              //           isChecked = newvalue!;
              //         });
              //       },
              //     ),
              //     Text('Do you agree terms and policy?')
              //   ],
              // )
            ],
          ),
        ),
      ),
    );
  }

  void _submitdata() {
    if (isChecked == false) {
      print('Agree the policy....');
      return;
    }
    if (_fkey!.currentState!.validate()) {
      //you can send the data to backend using api
      print('Thanks Customer ....');
    }
  }
}
