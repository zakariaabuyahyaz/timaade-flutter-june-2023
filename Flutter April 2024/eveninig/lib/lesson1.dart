import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson1 extends StatelessWidget {
  const Lesson1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.blue.shade200,
      // backgroundColor: Colors.blue[200],
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                // padding: EdgeInsets.only(left: 50, top: 50),
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                color: Colors.amber.shade400,
                child: Text(
                  textAlign: TextAlign.start,
                  'ONE',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // padding: EdgeInsets.only(left: 50, top: 50),
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                color: Colors.red,
                child: Text(
                  textAlign: TextAlign.start,
                  'TWO',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                'SAMPLE',
                style: TextStyle(fontSize: 15),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                // padding: EdgeInsets.only(left: 50, top: 50),
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                color: Colors.amber.shade400,
                child: Text(
                  textAlign: TextAlign.start,
                  'THREE',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                // padding: EdgeInsets.only(left: 50, top: 50),
                margin: EdgeInsets.all(5.0),
                width: 100,
                height: 100,
                color: Colors.red,
                child: Text(
                  textAlign: TextAlign.start,
                  'FOUR',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
