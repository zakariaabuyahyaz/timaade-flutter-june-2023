import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class Lesson6 extends StatefulWidget {
  const Lesson6({super.key});

  @override
  State<Lesson6> createState() => _Lesson6State();
}

class _Lesson6State extends State<Lesson6> {
  final _fkey = GlobalKey<FormState>();
  String _name = '';
  String _mobile = '';
  String _email = '';
  String _dob = '';
  String _password = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(
          'Register',
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30, 50, 30, 5),
        child: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          key: _fkey,
          child: ListView(
            children: [
              TextFormField(
                onSaved: (newValue) {
                  _name = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Fullname is Required";
                  }

                  if (newvalue!.length <= 10) {
                    return "Invalid Name";
                  }
                  return null;
                },
                keyboardType: TextInputType.name,
                maxLength: 35,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.person,
                      color: Colors.purple,
                    ),
                    labelText: 'Fullname',
                    hintText: 'Enter first, second and third names',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _email = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Email is Required";
                  }

                  return null;
                },
                keyboardType: TextInputType.emailAddress,
                maxLength: 20,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.mail,
                      color: Colors.purple,
                    ),
                    labelText: 'Email',
                    hintText: 'Use gmail or hotmail',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _mobile = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Mobile is Required";
                  }

                  if (newvalue!.length != 9) {
                    return "Invalid Mobile";
                  }
                  return null;
                },
                keyboardType: TextInputType.phone,
                maxLength: 9,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.phone,
                      color: Colors.purple,
                    ),
                    labelText: 'Mobile',
                    hintText: 'Enter 9 digits, EX: 63/65xxxxxxx',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _dob = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "DOB is Required";
                  }
                  return null;
                },
                keyboardType: TextInputType.datetime,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.calendar_month,
                      color: Colors.purple,
                    ),
                    labelText: 'DOB',
                    hintText: 'Format = yyyy-mm-dd',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _password = newValue!;
                },
                obscureText: true,
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Password is Required";
                  }

                  if (newvalue!.length < 5) {
                    return "Invalid Password";
                  }
                  return null;
                },
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.auto,
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(
                      Icons.key,
                      color: Colors.purple,
                    ),
                    labelText: 'Password',
                    hintText: 'Minimum 5 chars',
                    hintStyle: TextStyle(
                      color: Colors.purple,
                    )),
              ),

              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.purple),
                    onPressed: _submitdata,
                    child: Text('Submit'),
                  ),
                ],
              ),

              // Row(
              //   children: [
              //     Checkbox(
              //       value: isChecked,
              //       onChanged: (newvalue) {
              //         setState(() {
              //           isChecked = newvalue!;
              //         });
              //       },
              //     ),
              //     Text('Do you agree terms and policy?')
              //   ],
              // )
            ],
          ),
        ),
      ),
    );
  }

  void _submitdata() async {
    if (_fkey!.currentState!.validate()) {
      _fkey!.currentState!.save();

      //to send data to backend api
      Map<String, dynamic> newperson = {
        "name": _name,
        "dob": _dob,
        "email": _email,
        "mobile": _mobile,
        "password": _password,
        "gender": 'male'
      };

      final s_json = jsonEncode(newperson);
      String _url =
          'http://109.199.125.207/academy_employee_api/public/index.php/api/user/create';
      final response = await http.post(
        Uri.parse(_url),
        headers: {'content-type': 'application/json'},
        body: s_json,
      );

      print(s_json);
      print(response.body);

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.blue,
            content: Text(
              'Success, you are registered',
              style: TextStyle(color: Colors.white),
            )));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.red,
            content: Text(
              'Failure, please try again',
              style: TextStyle(color: Colors.white),
            )));
      }
    }
  }
}
