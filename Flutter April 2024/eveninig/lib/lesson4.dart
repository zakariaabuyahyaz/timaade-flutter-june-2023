import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class lesson4 extends StatefulWidget {
  const lesson4({super.key});

  @override
  State<lesson4> createState() => _lesson4State();
}

class _lesson4State extends State<lesson4> {
  double width = 50;
  double heigth = 50;
  double fontsize = 10;
  String C_Color = "red";
  int counter = 1;
  String direction = "increment";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lesson 4')),
      body: Column(
        children: [
          Container(
            alignment: Alignment.center,
            width: width,
            height: heigth,
            color: C_Color == "red" ? Colors.red : Colors.blue,
            child: Text(
              '$counter',
              style: TextStyle(fontSize: fontsize, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          ElevatedButton(
              onPressed: () {
                if (direction == "increment") {
                  counter++;
                  width = width + 10;
                  heigth = heigth + 10;
                  fontsize = fontsize + 3;
                } else {
                  counter--;
                  width = width - 10;
                  heigth = heigth - 10;
                  fontsize = fontsize - 3;
                }

                C_Color = C_Color == "red" ? "blue" : "red";

                if (counter > 20) {
                  direction = "decrement";
                }

                if (counter == 1) {
                  direction = "increment";
                }
                setState(() {}); // to update the UI
              },
              child: Text('Increment')),
        ],
      ),
    );
  }
}
