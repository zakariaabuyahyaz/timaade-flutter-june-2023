import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class lesson3 extends StatefulWidget {
  const lesson3({super.key});

  @override
  State<lesson3> createState() => _lesson3State();
}

class _lesson3State extends State<lesson3> {
  int counter = 0;
  bool is_post_visible = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        title: Text('Lesson 3'),
      ),
      body: Column(
        children: [
          //card
          Card(
            elevation: 20,
            shadowColor: Colors.red,
            color: Colors.blue.shade300,
            child: Column(
              children: [
                Text(
                  'Account Balance',
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'USD 23.5',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.red),
                ),
              ],
            ),
          ),

          SizedBox(
            height: 50,
          ),
          if (is_post_visible)
            Card(
              color: Colors.white,
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Column(
                  children: [
                    ListTile(
                      leading: CircleAvatar(
                          backgroundColor: Colors.purple, radius: 25),
                      trailing: Icon(
                        Icons.menu,
                        size: 20,
                        color: Colors.grey,
                      ),
                      title: Text(
                        'Khaalid Foodhaadhi',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '2024-05-26 11:23 PM',
                            style: TextStyle(fontSize: 13),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                '23 min ago',
                                style: TextStyle(fontSize: 13),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Icon(
                                Icons.public,
                                size: 15,
                                color: Colors.grey,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      textAlign: TextAlign.justify,
                      'If the [style] argument is null, the text will use the style from the closest enclosing [DefaultTextStyle].If the [style] argument is null, the text will use the style from the closest enclosing [DefaultTextStyle].If the [style] argument is null, the text will use the style from the closest enclosing [DefaultTextStyle].If the [style] argument is null, the text will use the style from the closest enclosing [DefaultTextStyle].If the [style] argument is null, the text will use the style from the closest enclosing [DefaultTextStyle].',
                      style: TextStyle(fontSize: 13, color: Colors.black),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: double.infinity,
                      height: 100,
                      color: Colors.orange,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.thumb_up_alt,
                          color: Colors.grey,
                          size: 40,
                        ),
                        Icon(
                          Icons.heart_broken,
                          color: Colors.grey,
                          size: 40,
                        ),
                        Icon(
                          Icons.comment,
                          color: Colors.grey,
                          size: 40,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),

          ElevatedButton(
            onPressed: () {
              print('old value is: $counter');
              counter++;
              if (counter >= 15 && counter <= 20) {
                is_post_visible = true;
              } else {
                is_post_visible = false;
              }
              print('new value is: $counter');
              setState(() {});
            },
            child: Text('Incement'),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Counter is $counter',
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
