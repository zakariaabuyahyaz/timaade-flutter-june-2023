import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class classwork21 extends StatelessWidget {
  const classwork21({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Hassan Profile')),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                width: double.infinity,
                height: 200,
                color: Colors.orange,
                child: Center(
                  child: Text(
                    'Hassan Profile',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: double.infinity,
                height: 400,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 80,
                    ),
                    Text(
                      'Hassan Galleydh',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      'Madaxweynaha soomaaliland ayaa maanta saxeexey warqada doorashada ee 2024. waxaa labilaabayaa ololaha doorashada ee wadanka oo dhan si loo helo .....',
                      style: TextStyle(fontSize: 14),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('200 Likes'),
                        ElevatedButton(
                          onPressed: () {
                            print('Increment likes');
                          },
                          child: Text('Add Likes'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            left: 155,
            top: 160,
            child: Container(
              width: 80,
              height: 80,
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
