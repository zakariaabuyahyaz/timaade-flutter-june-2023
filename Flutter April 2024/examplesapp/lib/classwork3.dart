import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Classwork3 extends StatefulWidget {
  const Classwork3({super.key});

  @override
  State<Classwork3> createState() => _Classwork3State();
}

class _Classwork3State extends State<Classwork3> {
  Color CColor = Colors.red;

  void changeHandler() {
    CColor = CColor == Colors.red ? Colors.blue : Colors.red;
    setState(() {}); //rebuilds the UI
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              width: 100,
              height: 100,
              color: CColor,
            ),
            SizedBox(
              height: 100,
            ),
            ElevatedButton(
              onPressed: changeHandler,
              child: Text('Change Color'),
            ),
          ],
        ),
      ),
    );
  }
}
