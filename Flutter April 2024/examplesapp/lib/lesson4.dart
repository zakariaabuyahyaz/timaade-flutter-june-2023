import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson4 extends StatefulWidget {
  const Lesson4({super.key});

  @override
  State<Lesson4> createState() => _Lesson4State();
}

class _Lesson4State extends State<Lesson4> {
  int counter = 1; //state variable
  double width = 50;
  double heigth = 50;
  double fontsize = 10;
  String C_Color = "red";
  bool is_visible = true;

  @override
  Widget build(BuildContext context) {
    //UI
    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson 4'),
      ),
      body: Column(
        children: [
          if (is_visible == true)
            Container(
              alignment: Alignment.center,
              width: width,
              height: heigth,
              color: C_Color == "red" ? Colors.red : Colors.blue,
              child: Text(
                '$counter',
                style:
                    TextStyle(fontSize: fontsize, fontWeight: FontWeight.bold),
              ),
            ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () {
                C_Color = C_Color == "red" ? "blue" : "red";
                width = width + 10;
                heigth = heigth + 10;
                fontsize = fontsize + 3;
                counter++;
                if (counter >= 15 && counter <= 20) {
                  is_visible = false;
                } else {
                  is_visible = true;
                }
                setState(() {});
              },
              child: Text('Increment'))
        ],
      ),
    );
  }
}
