import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class LessonFour extends StatefulWidget {
  const LessonFour({super.key});

  @override
  State<LessonFour> createState() => _LessonFourState();
}

class _LessonFourState extends State<LessonFour> {
  final _fkey = GlobalKey<FormState>();
  List<String> allCountries = ['Select', 'Somaliland', 'Somalia', 'Ethiopia'];
  String selectedCountry = 'Select';
  bool isChecked = false;

  void submitFunc() {
    if (_fkey!.currentState!.validate()) {
      print('Thanks Customer ....');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(
          'Sing up',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Form(
          autovalidateMode: AutovalidateMode.always,
          key: _fkey,
          child: ListView(
            children: [
              TextFormField(
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Fullname is Required";
                  }

                  if (newvalue!.length < 10) {
                    return "Invalid fullname";
                  }
                  return null;
                },
                keyboardType: TextInputType.name,
                maxLength: 15,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Fullname',
                  hintText: 'Enter first, second and third name',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.person,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Mobile is Required";
                  }

                  if (newvalue!.length < 9) {
                    return "Invalid Mobile";
                  }
                  return null;
                },
                keyboardType: TextInputType.phone,
                maxLength: 9,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Mobile',
                  hintText: 'Start with 63/65. Ex: 65xxxxxxx',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.phone,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              DropdownButtonFormField(
                validator: (newvalue) {
                  if (newvalue == 'Select') {
                    return "Country is Required";
                  }

                  return null;
                },
                decoration: InputDecoration(
                  labelText: 'Country',
                  prefixIcon: Icon(Icons.location_city),
                  border: OutlineInputBorder(),
                ),
                items: allCountries.map((element) {
                  return DropdownMenuItem(
                    child: Text(element),
                    value: element,
                  );
                }).toList(),
                value: selectedCountry,
                onChanged: (newvalue) {
                  setState(() {
                    selectedCountry = newvalue!;
                  });
                },
              ),
              SizedBox(
                height: 15,
              ),
              CheckboxListTile(
                title: Text('Policy'),
                subtitle: Text(
                    'This software uses list of terms that need to be accepted. if you dont accept your account will be invalid'),
                value: isChecked,
                onChanged: (newvalue) {
                  setState(() {
                    isChecked = newvalue!;
                  });
                },
              ),
              // Row(
              //   children: [
              //     Checkbox(
              //       value: isChecked,
              //       onChanged: (newvalue) {
              //         setState(() {
              //           isChecked = newvalue!;
              //         });
              //       },
              //     ),
              //     Text('Do you agree terms and policy?'),
              //   ],
              // ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple),
                      onPressed: submitFunc,
                      child: Text('Sign Up')),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
