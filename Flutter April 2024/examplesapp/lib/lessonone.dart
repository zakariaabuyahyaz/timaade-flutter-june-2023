import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class LessonOne extends StatelessWidget {
  const LessonOne({super.key});

  String calculate_Percentage() {
    return "20%";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      // backgroundColor: Colors.blue.shade200,
      body: Column(
        children: [
          // Text(
          //   'Welcome to Flutter',
          //   style: TextStyle(
          //     fontSize: 25.0,
          //     color: Colors.white,
          //     fontWeight: FontWeight.bold,
          //   ),
          // ),
          // Text('Flutter practical session'),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Container(
              //   margin: EdgeInsets.all(20.0),
              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //   width: 100,
              //   height: 100,
              //   color: Colors.amber.shade400,
              //   child: Text(
              //     '1',
              //     style: TextStyle(fontSize: 30.0),
              //   ),
              // ),
              // Row(
              //   children: [
              //     Container(
              //       margin: EdgeInsets.all(20.0),
              //       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //       width: 80,
              //       height: 100,
              //       color: Colors.amber.shade400,
              //       child: Text(
              //         '1.1',
              //         style: TextStyle(fontSize: 30.0),
              //       ),
              //     ),
              //     Container(
              //       margin: EdgeInsets.all(20.0),
              //       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //       width: 80,
              //       height: 100,
              //       color: Colors.amber.shade400,
              //       child: Text(
              //         '1.2',
              //         style: TextStyle(fontSize: 30.0),
              //       ),
              //     ),
              //     Container(
              //       margin: EdgeInsets.all(20.0),
              //       padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              //       width: 80,
              //       height: 100,
              //       color: Colors.amber.shade400,
              //       child: Text(
              //         '1.3',
              //         style: TextStyle(fontSize: 30.0),
              //       ),
              //     ),
              //   ],
              // ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                width: double.infinity,
                height: 200,
                color: Colors.purple,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '18 May',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Maanta madaxweynaha ayaa ka hadley taariikhdii qaran ka jamuuriyada soomaaliland. wuxuu ku nuuxnuusadey halgankii dheeraa ee.......',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                            onPressed: () {}, child: Text('Read Nore..')),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
