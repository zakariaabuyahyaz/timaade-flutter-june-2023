import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lesson3 extends StatelessWidget {
  const Lesson3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lesson Three')),
      body: Column(
        children: [
          //list tile widget
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.purple,
              radius: 25,
            ),
            title: Text(
              'Hassan Galleydh',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '2024-05-27',
                  style: TextStyle(fontSize: 13),
                ),
                Row(
                  children: [
                    Text(
                      '23 min Ago',
                      style: TextStyle(fontSize: 13),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.public_outlined,
                      size: 10,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ],
            ),
            trailing: Icon(
              Icons.menu,
              size: 20,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            color: Colors.blue,
            elevation: 5,
            shadowColor: Colors.black,
            child: Column(
              children: [
                Text('Account Balance'),
                SizedBox(
                  height: 15,
                ),
                Text(
                  '21.3 USD',
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
