import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Classwork1 extends StatelessWidget {
  const Classwork1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 100,
            height: 100,
            color: Colors.red,
            margin: EdgeInsets.all(10),
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
            margin: EdgeInsets.all(10),
          ),
        ],
      ),
    );
  }
}

class Classwork2 extends StatelessWidget {
  const Classwork2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(3),
            width: 200,
            height: 200,
            color: Colors.red,
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.all(3),
                width: 200,
                height: 200,
                color: Colors.red,
              ),
              Container(
                margin: EdgeInsets.all(3),
                width: 200,
                height: 200,
                color: Colors.red,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.all(3),
            width: 200,
            height: 200,
            color: Colors.red,
            child: Text(
              '18',
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}

class classwork3 extends StatelessWidget {
  const classwork3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        width: double.infinity,
        height: 250,
        color: Colors.purple.shade300,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Raisi Death',
              style: TextStyle(fontSize: 25, color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Madaxweynaha dalka Iran ayaa ku geeriyoodey shil diyaaradeed xili uu kasoo laabtey ......',
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Icon(
                  Icons.share_outlined,
                  color: Colors.white,
                  size: 30,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
