import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:http/http.dart' as http;

class Lessonsix extends StatefulWidget {
  const Lessonsix({super.key});

  @override
  State<Lessonsix> createState() => _LessonsixState();
}

class _LessonsixState extends State<Lessonsix> {
  final _fkey = GlobalKey<FormState>();
  String _msg = '';
  String _name = '';
  String _dob = '';
  String _email = '';
  String _mobile = '';
  String _password = '';

  void submitFunc() async {
    if (_fkey!.currentState!.validate()) {
      _fkey!.currentState!.save();
      //send the data to web server api

      //create map
      Map<String, dynamic> newuser = {
        "name": _name,
        "dob": _dob,
        "email": _email,
        "mobile": _mobile,
        "password": _password,
        "gender": 'female'
      };

      String jsonstring = jsonEncode(newuser);
      print(jsonstring);
      String _url =
          'http://109.199.125.207/academy_employee_api/public/index.php/api/user/create';
      final response = await http.post(Uri.parse(_url),
          headers: {'content-type': 'applicaiton/json'}, body: jsonstring);

      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.blue,
            content: Text(
              'Success, you registered',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            backgroundColor: Colors.red,
            content: Text(
              'Failed, try again',
              style: TextStyle(color: Colors.white),
            )));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(
          'Register',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Form(
          autovalidateMode: AutovalidateMode.always,
          key: _fkey,
          child: ListView(
            children: [
              TextFormField(
                onSaved: (newValue) {
                  _name = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Fullname is Required";
                  }

                  if (newvalue!.length < 10) {
                    return "Invalid fullname";
                  }
                  return null;
                },
                keyboardType: TextInputType.name,
                maxLength: 15,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Fullname',
                  hintText: 'Enter first, second and third name',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.person,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _mobile = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Mobile is Required";
                  }

                  if (newvalue!.length < 9) {
                    return "Invalid Mobile";
                  }
                  return null;
                },
                keyboardType: TextInputType.phone,
                maxLength: 9,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Mobile',
                  hintText: 'Start with 63/65. Ex: 65xxxxxxx',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.phone,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _email = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Email is Required";
                  }

                  return null;
                },
                keyboardType: TextInputType.emailAddress,
                maxLength: 20,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Email',
                  hintText: 'Use gmail or hotmail',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.mail,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _dob = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "DOB is Required";
                  }

                  return null;
                },
                keyboardType: TextInputType.datetime,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'DOB',
                  hintText: 'Format = yyyy-mm-dd',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.date_range,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onSaved: (newValue) {
                  _password = newValue!;
                },
                validator: (newvalue) {
                  if (newvalue!.isEmpty) {
                    return "Password is Required";
                  }

                  if (newvalue!.length <= 5) {
                    return "Invalid password";
                  }
                  return null;
                },
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Password',
                  hintText: 'Enter minimum 5 chars',
                  hintStyle: TextStyle(color: Colors.purple),
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.purple,
                    size: 20,
                  ),
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.purple),
                      onPressed: submitFunc,
                      child: Text('Sign Up')),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              // if (_msg != '')
              //   Text(
              //     _msg,
              //     style: TextStyle(color: Colors.red, fontSize: 30),
              //   ),
            ],
          ),
        ),
      ),
    );
  }
}
