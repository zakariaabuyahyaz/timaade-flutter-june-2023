import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class Lessontwo extends StatelessWidget {
  const Lessontwo({super.key});

//this build function is responsible to creating UI
  @override
  Widget build(BuildContext context) {
    final wid = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Stack Examples'),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.amber.shade200),
              Container(width: double.infinity, height: 250, color: Colors.red),
            ],
          ),
          Positioned(
            left: 210,
            top: 190,
            child: Container(
              width: 100,
              height: 100,
              color: Colors.white,
            ),
          ),
        ],
      ),
      // body: Column(
      //   children: [
      //     Stack(
      //       alignment: Alignment.topLeft,
      //       fit: StackFit.passthrough,
      //       children: [
      //         Container(
      //           alignment: Alignment.topLeft,
      //           width: double.infinity,
      //           height: 200,
      //           color: Colors.orange,
      //           child: Row(
      //             children: [
      //               Text(
      //                 'Profile',
      //                 style: TextStyle(fontSize: 25, color: Colors.black),
      //               ),
      //               Text(
      //                 'Amina Ahmed',
      //                 style: TextStyle(fontSize: 15, color: Colors.white),
      //               ),
      //             ],
      //           ),
      //         ),
      //         Positioned(
      //           left: 30,
      //           top: 120,
      //           child: Container(
      //             width: 350,
      //             height: 100,
      //             color: Colors.purple,
      //           ),
      //         ),
      //         // Text('18'),
      //       ],
      //     ),
      //     SizedBox(
      //       height: 150,
      //     ),
      //     Container(
      //       width: wid * 0.5,
      //       height: 80,
      //       color: Colors.red,
      //       child: Text(
      //         '$wid',
      //         style: TextStyle(fontSize: 25, color: Colors.white),
      //       ),
      //     )
      //   ],
      // ),
    ); // representing UI Screen
  }
}
