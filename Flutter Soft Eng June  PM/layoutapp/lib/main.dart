import 'package:flutter/material.dart';
import 'package:layoutapp/DropDownItems.dart';
import 'package:layoutapp/DisplayList.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: ListviewMoreExamples(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class ExampleOne extends StatelessWidget {
  const ExampleOne({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Container Exampple'),
        backgroundColor: Colors.blue[200],
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.topLeft,
        color: Colors.pink[300],
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        padding: EdgeInsets.all(5),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              width: 150,
              height: 150,
              color: Colors.blue,
              alignment: Alignment.topLeft,
              child: Text('Container 1'),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: 150,
              height: 150,
              alignment: Alignment.bottomRight,
              decoration: BoxDecoration(
                color: Colors.red,
                border: Border.all(color: Colors.green, width: 4),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text('Container 2'),
            ),
          ],
        ),
      ),
    );
  }
}

class ExampleTwo extends StatelessWidget {
  const ExampleTwo({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Row and Column')),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        padding: EdgeInsets.all(5),
        color: Colors.amber[200],
        alignment: Alignment.topLeft,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              color: Colors.lightBlue,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.red,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.green,
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.lightBlue,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.red,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.green,
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.lightBlue,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.red,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.yellow,
                  ),
                  Container(
                    width: 20,
                    height: 20,
                    color: Colors.green,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StackExamples extends StatelessWidget {
  const StackExamples({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('STack Examples')),
      body: Container(
        // alignment: Alignment.center,
        // width: double.infinity,
        // height: double.infinity,
        padding: EdgeInsets.all(5),
        child: Stack(
          fit: StackFit.loose,
          alignment: AlignmentDirectional.topEnd,
          children: [
            Container(
              width: 200,
              height: 200,
              color: Colors.blue,
            ),
            Container(
              width: 100,
              height: 100,
              color: Colors.red,
            ),
            Positioned(
              left: 70,
              top: 50,
              child: Text('Stack Ex'),
            ),
          ],
        ),
      ),
    );
  }
}

class ExerciseOne extends StatelessWidget {
  const ExerciseOne({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Exercise'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  // mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('First'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.blue,
                          child: Text('ONE'),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.red,
                          child: Text('TWO'),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  // mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Second'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.blue,
                          child: Text('ONE'),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.red,
                          child: Text('TWO'),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  // mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text('Third'),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.blue,
                          child: Text('ONE'),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: 150,
                          height: 150,
                          color: Colors.red,
                          child: Text('TWO'),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class FormFieldsExample extends StatefulWidget {
  const FormFieldsExample({super.key});

  @override
  State<FormFieldsExample> createState() => _FormFieldsExampleState();
}

class _FormFieldsExampleState extends State<FormFieldsExample> {
  final _myformkey = GlobalKey<FormState>();
  TextEditingController _fullnameController = TextEditingController();
  TextEditingController _mobileController = TextEditingController();
  String? message;

  bool ischecked = false;
  //dropdown config
  String dropInitialValue = "Select";
  List<String> dropItems = ["Select", "ICT", "BA", "Medicine", "Others"];
  String? dropSelectedvalue;

  //university
  List<MyDropDownItem> listOfUniversity = [
    MyDropDownItem(id: "", name: "Select"),
    MyDropDownItem(id: "U01", name: "Timaade"),
    MyDropDownItem(id: "U02", name: "UOH"),
    MyDropDownItem(id: "U03", name: "Golis"),
  ];
  late MyDropDownItem selectedUniversity;
  // MyDropDownItem? universityInitialValue =
  //     listOfUniversity.isNotEmpty ? listOfUniversity[0] : null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Form Examples')),
      body: ListView(children: [
        Container(
          // width: double.infinity,
          // height: double.infinity,
          padding: EdgeInsets.all(20),
          child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _myformkey,
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),

                /// this widget is used to give spacing vertically or horizentally
                Text(
                  'Student Register',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.underline),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: _fullnameController,
                  decoration: InputDecoration(
                    labelText: 'Full Name',
                    // floatingLabelBehavior: FloatingLabelBehavior.never,
                    hintText: 'Gali magacaaga oo afaran',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    prefixIcon: Icon(
                      Icons.person_2_outlined,
                      color: Colors.blue,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Fullname is required";
                    }
                    if (value.length < 14) {
                      return "Invalid Name";
                    }

                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _mobileController,
                  decoration: InputDecoration(
                    labelText: 'Mobile',
                    // floatingLabelBehavior: FloatingLabelBehavior.never,
                    hintText: 'Gali 9 god oo number ah',
                    hintStyle: TextStyle(
                      color: Colors.black,
                    ),
                    prefixIcon: Icon(
                      Icons.phone,
                      color: Colors.blue,
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Mobile is required";
                    }
                    if (value.length != 9) {
                      return "Invalid Mobile";
                    }

                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField<String>(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'Faculty',
                      prefixIcon: Icon(
                        Icons.cast_for_education_outlined,
                        color: Colors.blue,
                      )),
                  value: dropInitialValue,
                  items: dropItems.map((element) {
                    return DropdownMenuItem<String>(
                        child: Text(element), value: element);
                  }).toList(),
                  onChanged: (value) {
                    dropSelectedvalue = value;
                  },
                  validator: (value) {
                    if (value == "Select") {
                      return "Faculty is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                DropdownButtonFormField<MyDropDownItem>(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      labelText: 'University',
                      prefixIcon: Icon(
                        Icons.cast_for_education_outlined,
                        color: Colors.blue,
                      )),
                  value:
                      listOfUniversity.isNotEmpty ? listOfUniversity[0] : null,
                  items: listOfUniversity.map((element) {
                    return DropdownMenuItem<MyDropDownItem>(
                        child: Text(element.name), value: element);
                  }).toList(),
                  onChanged: (value) {
                    selectedUniversity = value!;
                  },
                  validator: (value) {
                    if (value?.id == "") {
                      return "University is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Checkbox(
                      value: ischecked,
                      onChanged: (newvalue) {
                        setState(() {
                          ischecked = newvalue!;
                        });
                      },
                    ),
                    Text('Policy and terms, do you agree?'),
                  ],
                ),

                SizedBox(
                  height: 10,
                ),
                CheckboxListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text('Policy'),
                  subtitle: Text('More info about this plicy goes here.'),
                  value: ischecked,
                  onChanged: (value) {
                    setState(() {
                      ischecked = value!;
                    });
                  },
                  secondary: Icon(Icons.book_outlined, color: Colors.blue),
                ),
                Card(
                  elevation: 4,
                  color: Colors.orangeAccent,
                  child: Column(children: [
                    Text('Card Data'),
                    TextButton(onPressed: () {}, child: Text('Click here')),
                  ]),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                      ),
                      onPressed: () {
                        if (_myformkey.currentState!.validate()) {
                          //reading data from the textboxes
                          String _fname = _fullnameController.text;
                          int Mobile = int.parse(_mobileController.text);

                          message = 'validation passed with this data. name:' +
                              _fname +
                              ', mobile=' +
                              Mobile.toString() +
                              ', Faculty=' +
                              dropSelectedvalue!;
                          setState(() {});
                        } else {
                          print('Validation failed');
                        }
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.send_rounded,
                            color: Colors.white,
                          ),
                          Text('Submit')
                        ],
                      ),
                    ),
                  ],
                ),
                Text(message ?? ""),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}

class ListviewMoreExamples extends StatefulWidget {
  const ListviewMoreExamples({super.key});

  @override
  State<ListviewMoreExamples> createState() => _ListviewMoreExamplesState();
}

class _ListviewMoreExamplesState extends State<ListviewMoreExamples> {
  List<String> items = ["Item one", "Item two", "Item Three"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ListView Examples')),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Card(
            color: Colors.orange[50],
            elevation: 2,
            child: ListTile(
              title: Text(items[index]),
              subtitle: Text('More info goes here'),
              // leading: Text('${index + 1}'),
              trailing: Icon(Icons.menu),
              onTap: () {
                print('your selected ${items[index]}');
              },
            ),
          );
        },
      ),
    );
  }
}
