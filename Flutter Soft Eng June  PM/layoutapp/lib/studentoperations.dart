import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class StudentOperations {
  int id;
  String Name;
  String Email;

  StudentOperations(
      {required this.id, required this.Name, required this.Email});

  static Future<List<dynamic>> GetStudents() async {
    final response =
        await http.get(Uri.parse('https://jsonplaceholder.typicode.com/users'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load students');
    }
  }
}
