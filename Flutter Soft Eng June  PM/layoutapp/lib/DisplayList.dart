import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:layoutapp/studentoperations.dart';

class DisplayList extends StatefulWidget {
  const DisplayList({super.key});

  @override
  State<DisplayList> createState() => _DisplayListState();
}

class _DisplayListState extends State<DisplayList> {
  List<dynamic> students = [];
  List<dynamic> filteredstudents = [];

  Future<void> loadData() async {
    students = await StudentOperations.GetStudents();
    filteredstudents = students;
    setState(() {}); //
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Students'),
        actions: [
          PopupMenuButton<String>(
            icon: Icon(Icons.menu),
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text('New'),
                  value: 'new',
                ),
                PopupMenuItem(
                  child: Text('Category'),
                  value: 'category',
                ),
              ];
            },
            onSelected: (value) {
              if (value == "new") {
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => UserFormScreen()));
              } else if (value == "category") {
                // getData();
              }
            },
          )
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              // onChanged: (value) => searchStudents(value),
              decoration: InputDecoration(
                hintText: 'Search by name',
                prefixIcon: Icon(Icons.search),
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: filteredstudents.length,
              itemBuilder: (context, index) {
                final student = filteredstudents[index];
                return Card(
                  elevation: 2,
                  child: ListTile(
                    title: Text(student['name']),
                    subtitle: Text(student['email']),
                    trailing: PopupMenuButton(
                      itemBuilder: (BuildContext context) {
                        return [
                          PopupMenuItem(
                            child: Text('Edit'),
                            value: 'edit',
                          ),
                          PopupMenuItem(
                            child: Text('Delete'),
                            value: 'delete',
                          ),
                          PopupMenuItem(
                            child: Text('View Profile'),
                            value: 'profile',
                          ),
                        ];
                      },
                      onSelected: (String action) {
                        // Perform the action based on the selected option
                        if (action == 'edit') {
                          // User pdata = User(
                          //     id: student["id"],
                          //     name: student["name"],
                          //     email: student["email"]);
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) =>
                          //             UserFormScreen(user: pdata)));
                        } else if (action == 'delete') {
                          // Implement deleting logic
                          // Example: deleteStudent(student['id']);
                        } else if (action == 'profile') {
                          // Implement viewing profile logic
                          // Example: Navigator.push(context, MaterialPageRoute(builder: (context) => UserFormScreen(pdata)));
                        }
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
